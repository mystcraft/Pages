package pages;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import pages.proxy.CommonProxy;

@Mod(modid = Pages.MOD_ID, name = Pages.NAME, version = Pages.VERSION, dependencies = Pages.DEPENDENCIES)
public class Pages {

    public static final String MOD_ID = "pages";
    public static final String NAME = "Pages";
    public static final String VERSION = "2.0.0.1";
    public static final String DEPENDENCIES = "required-after:mystlibrary";
    public static final String CLIENT_PROXY_CLASS = "pages.proxy.ClientProxy";
    public static final String SERVER_PROXY_CLASS = "pages.proxy.ServerProxy";
    public static final Gson GSON = buildJson();

    @SidedProxy(clientSide = Pages.CLIENT_PROXY_CLASS, serverSide = Pages.SERVER_PROXY_CLASS)
    public static CommonProxy proxy;

    public static Logger logger;

    @EventHandler
    public static void preInit(FMLPreInitializationEvent event) throws IOException, URISyntaxException {
        logger = event.getModLog();
        proxy.preInit(event);
    }

    @EventHandler
    public static void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @EventHandler
    public static void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }

    public static ResourceLocation resource(String path) {
        return new ResourceLocation(MOD_ID, path);
    }

    public static Gson buildJson() {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        return builder.create();
    }

    /**
     * Tweak and use to update the symbol JSONs when necessary. Currently set to
     * update blocks from Myst Library 0.0.1.0 to 0.0.2.0.
     */
    @SideOnly(Side.CLIENT)
    public static void updateJson() {
        BufferedReader reader;
        String[] names = new String[] { "appliedenergistics2", "biomesoplenty", "botania", "enderio", "extrautils2",
                "immersiveengineering", "rftools", "tconstruct", "thaumcraft", "thermalfoundation", "twilightforest",
                "vanilla" };
        for (String name : names) {
            try {
                reader = new BufferedReader(new InputStreamReader(Pages.class.getClassLoader()
                        .getResourceAsStream("assets/pages/agesymbols/blocks_" + name + ".json"), "UTF-8"));
                JsonArray inputArray = GSON.fromJson(reader, JsonArray.class);
                JsonArray outputArray = new JsonArray();
                for (JsonElement symbolElement : inputArray) {
                    JsonObject inputSymbol = symbolElement.getAsJsonObject();
                    String modIdSymbol = inputSymbol.get("modid").getAsString();
                    String identifierSymbol = inputSymbol.get("identifier").getAsString();
                    String symbolId = modIdSymbol + ":" + identifierSymbol;
                    JsonArray poem = inputSymbol.get("poem").getAsJsonArray();
                    Integer cardRank = inputSymbol.get("card_rank").getAsInt();
                    JsonArray grammarRules = inputSymbol.get("grammar_rules").getAsJsonArray();
                    JsonObject localization = inputSymbol.get("localization").getAsJsonObject();
                    JsonObject instability = inputSymbol.get("instability").getAsJsonObject();
                    JsonObject inputBehavior = inputSymbol.get("behavior").getAsJsonObject();
                    String modIdBehavior = inputBehavior.get("modid").getAsString();
                    String identifierBehavior = inputBehavior.get("identifier").getAsString();
                    String behaviorId = modIdBehavior + ":" + identifierBehavior;
                    String blockId = inputBehavior.get("blockid").getAsString();
                    JsonObject properties = inputBehavior.get("properties") == null ? null
                            : inputBehavior.get("properties").getAsJsonObject();
                    Integer instabilityBase = inputBehavior.get("instability_base") == null ? null
                            : inputBehavior.get("instability_base").getAsInt();
                    Integer instabilityExposed = inputBehavior.get("instability_exposed") == null ? null
                            : inputBehavior.get("instability_exposed").getAsInt();
                    JsonObject outputBehavior = new JsonObject();
                    outputBehavior.addProperty("behavior_id", behaviorId);
                    outputBehavior.addProperty("block_id", blockId);
                    if (properties != null)
                        outputBehavior.add("properties", properties);
                    if (instabilityBase != null)
                        outputBehavior.addProperty("instability_base", instabilityBase);
                    if (instabilityExposed != null)
                        outputBehavior.addProperty("instability_exposed", instabilityExposed);
                    JsonObject outputSymbol = new JsonObject();
                    outputSymbol.addProperty("symbol_id", symbolId);
                    outputSymbol.add("poem", poem);
                    if (cardRank != null)
                        outputSymbol.addProperty("card_rank", cardRank);
                    outputSymbol.add("grammar_rules", grammarRules);
                    if (instability != null)
                        outputSymbol.add("instability", instability);
                    outputSymbol.add("localization", localization);
                    outputSymbol.add("behavior", outputBehavior);
                    outputArray.add(outputSymbol);

                    try {
                        File file = new File("C:/updated/blocks_" + name + ".json");
                        if (file.exists()) {
                            file.delete();
                        }
                        file.createNewFile();
                        FileWriter writer = new FileWriter(file);
                        Pages.GSON.toJson(outputArray, writer);
                        writer.close();
                    } catch (JsonIOException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

}
