package pages.item;

import com.xcompwiz.mystcraft.core.MystcraftCommonProxy;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import pages.Pages;
import pages.network.PagesPacketHandler;

/**
 * An item to assist in the process of creating new block symbols/pages. It will
 * save any right-clicked block as a JSON file that will be loaded as a
 * symbol/page the next time the game is launched.
 */
public class ItemBlockWriter extends Item {

    public static final String ID = Pages.MOD_ID + ":block_writer";

    public ItemBlockWriter() {
        this.setRegistryName(ID);
        this.setMaxStackSize(1);
        this.setCreativeTab(MystcraftCommonProxy.tabMystCommon);
        this.setUnlocalizedName(Pages.MOD_ID + ".block_writer");
    }

    /**
     * Collects all pertinent block info and makes a request to save it as a symbol
     * using Myst Library's JSON format.
     */
    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing facing,
            float hitX, float hitY, float hitZ) {
        if (!world.isRemote) {
            IBlockState state = world.getBlockState(pos);
            PagesPacketHandler.sendBlockStateToClient((EntityPlayerMP) player, state);
        }
        return EnumActionResult.PASS;
    }

}