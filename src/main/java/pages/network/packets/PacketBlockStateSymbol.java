package pages.network.packets;

import java.nio.charset.Charset;
import java.util.ArrayList;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.netty.buffer.ByteBuf;
import mystlibrary.exception.ModMissingException;
import mystlibrary.exception.SymbolBuildException;
import mystlibrary.symbol.SymbolMetadata;
import mystlibrary.symbol.behavior.BlockStateBehavior;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.init.Blocks;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import pages.Pages;
import pages.client.gui.GuiScreenBlockWriter;

public class PacketBlockStateSymbol implements IMessage {

    IBlockState blockState;

    public PacketBlockStateSymbol(IBlockState state) {
        this.blockState = state;
    }

    public PacketBlockStateSymbol() {
        this.blockState = Blocks.AIR.getDefaultState();
    }

    public static class Handler implements IMessageHandler<PacketBlockStateSymbol, IMessage> {

        @SideOnly(Side.CLIENT)
        @Override
        public IMessage onMessage(PacketBlockStateSymbol msg, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(
                    () -> Minecraft.getMinecraft().displayGuiScreen(new GuiScreenBlockWriter(msg.blockState)));
            return null;
        }
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        String stateString = buf.readCharSequence(buf.readableBytes(), Charset.defaultCharset()).toString();
        JsonObject stateObject = new JsonParser().parse(stateString).getAsJsonObject();
        SymbolMetadata metadata = new SymbolMetadata();
        metadata.cardRank = 0;
        metadata.grammarRules = new ArrayList<>();
        metadata.instabilityPerInstance = 0;
        metadata.localizationParameters = new ArrayList<>();
        metadata.maxCount = 0;
        metadata.name = Pages.resource("transmitted_block");
        metadata.poem = new ArrayList<>();
        metadata.unlocalName = "";
        try {
            this.blockState = ((BlockStateBehavior) BlockStateBehavior.fromJson(stateObject,
                    metadata)).blockDescriptor.blockstate;
        } catch (SymbolBuildException | ModMissingException.BlockSymbol e) {
            e.printStackTrace();
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        JsonObject stateObject = new BlockStateBehavior(this.blockState, new ArrayList<>(), 0.0F, 0.0F).toJson();
        String stateString = stateObject.toString();
        buf.writeCharSequence(stateString, Charset.defaultCharset());
    }

}
