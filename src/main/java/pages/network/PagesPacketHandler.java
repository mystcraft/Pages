package pages.network;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import pages.Pages;
import pages.network.packets.PacketBlockStateSymbol;

public class PagesPacketHandler {

    public static final SimpleNetworkWrapper CHANNEL = NetworkRegistry.INSTANCE.newSimpleChannel(Pages.MOD_ID);

    private static int index = 0;

    public static void register() {
        CHANNEL.registerMessage(PacketBlockStateSymbol.Handler.class, PacketBlockStateSymbol.class, index++,
                Side.CLIENT);
    }

    public static void sendBlockStateToClient(EntityPlayerMP player, IBlockState state) {
        CHANNEL.sendTo(new PacketBlockStateSymbol(state), player);
    }

}