package pages.save;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.annotation.Nonnull;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;

import pages.Pages;

/**
 * Provides methods for generating and/or saving a blockstate in JSON format.
 */
public final class SymbolSaver {

    private SymbolSaver() {
    }

    /**
     * Converts a symbol to JSON and saves it to disk.
     * 
     * @param symbol The symbol to be saved
     * @param file The file to be created.
     * @param overwrite Whether the file should be overwritten if it already exists.
     * @return
     */
    public static boolean saveSymbolToDisk(@Nonnull JsonObject jsonObject, @Nonnull File file, boolean overwrite) {
        if (file.exists() && !overwrite) {
            return false;
        }
        JsonArray jsonArray = new JsonArray();
        jsonArray.add(jsonObject);
        try {
            FileWriter writer = new FileWriter(file);
            Pages.GSON.toJson(jsonArray, writer);
            writer.close();
            return true;
        } catch (JsonIOException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
