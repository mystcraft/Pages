package pages.client.gui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.input.Keyboard;

import com.google.common.collect.Iterables;
import com.google.gson.JsonObject;
import com.xcompwiz.mystcraft.api.symbol.BlockCategory;

import mystlibrary.localization.Localization;
import mystlibrary.symbol.behavior.BlockStateBehavior;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import pages.Pages;
import pages.client.gui.element.GuiElementBlockPropertiesEditor;
import pages.client.gui.element.GuiElementBlockWriterListener;
import pages.client.gui.element.GuiElementGrammarRuleEditor;
import pages.client.gui.element.GuiElementListItemGrammarRule;
import pages.client.gui.element.GuiElementPoemEditor;
import pages.client.gui.element.generic.GuiElement;
import pages.client.gui.element.generic.GuiElementButton;
import pages.client.gui.element.generic.GuiElementString;
import pages.client.gui.element.generic.GuiElementTab;
import pages.client.gui.element.generic.GuiElementTextFieldLabeled;
import pages.client.gui.element.generic.GuiElementWindow;
import pages.client.gui.element.generic.keyvalidator.KeyValidators;

@SideOnly(Side.CLIENT)
public class GuiScreenBlockWriter extends GuiScreenBase {

    public IBlockState blockState;
    public int guiWidth;
    public int guiHeight;
    public int guiLeft;
    public int guiTop;

    protected Collection<BlockCategory> blockCategories = new ArrayList<BlockCategory>(BlockCategory.getCategories());

    protected BlockStateBehavior behavior;
    protected JsonObject blockJson;

    public GuiElementTab tabGeneral;
    public GuiElementTab tabGrammar;
    public GuiElementTab tabBlock;
    public GuiElementTextFieldLabeled symbolIdField;
    public GuiElementPoemEditor poemEditor;
    public GuiElementTextFieldLabeled cardRankField;
    public GuiElementGrammarRuleEditor grammarRuleEditor;
    public GuiElementBlockPropertiesEditor propertiesEditor;
    public GuiElementTextFieldLabeled baseInstabilityField;
    public GuiElementTextFieldLabeled exposedInstabilityField;

    public GuiScreenBlockWriter(IBlockState state) {
        this.blockState = state;
        this.guiWidth = 256;
        this.guiHeight = 130;
        this.blockCategories.remove(BlockCategory.ANY);
        this.behavior = new BlockStateBehavior(state, new ArrayList<BlockCategory>(), 0.0F, 0.0F);
        this.blockJson = this.behavior.toJson();
    }

    @Override
    public void initGui() {

        super.initGui();

        // Allows the user to get repeated results when holding down a key.
        Keyboard.enableRepeatEvents(true);

        this.guiLeft = (this.width - this.guiWidth) / 2;
        this.guiTop = (this.height - this.guiHeight + 18) / 2;

        // Add window element.
        this.addElement(new GuiElementWindow(ElementIds.WINDOW.ordinal(), this.guiLeft, this.guiTop, this.guiWidth,
                this.guiHeight, null, null, 0.0F));

        GuiElement screenListener = this.addElement(new GuiElementBlockWriterListener(
                ElementIds.SCREEN_LISTENER.ordinal(), 0, 0, 0, 0, null, null, this, zLevel));

        // Add tab elements.
        String blockString = new TextComponentTranslation("gui.pages.block_writer.tab.block").getFormattedText();
        this.tabBlock = new GuiElementTab(ElementIds.TAB_BLOCK.ordinal(), 0, 0, null, Arrays.asList(screenListener),
                false, true,
                new GuiElementString(0, 0, 0, null, null, this.mc, blockString, Color.BLACK.getRGB(), 0.0F), 0.0F);
        tabBlock.x = this.guiLeft + this.guiWidth - tabBlock.width;
        tabBlock.y = this.guiTop - tabBlock.height;
        tabBlock = new GuiElementTab(tabBlock.id, tabBlock.x, tabBlock.y, tabBlock.validator, tabBlock.listeners,
                tabBlock.onLeftSide, tabBlock.onRightSide,
                new GuiElementString(0, 0, 0, null, null, this.mc, blockString, Color.BLACK.getRGB(), 0.0F), 0.0F);
        this.addElement(tabBlock);

        String grammarString = new TextComponentTranslation("gui.pages.block_writer.tab.grammar").getFormattedText();
        this.tabGrammar = new GuiElementTab(ElementIds.TAB_GRAMMAR.ordinal(), 0, 0, null, Arrays.asList(screenListener),
                false, false,
                new GuiElementString(0, 0, 0, null, null, this.mc, grammarString, Color.BLACK.getRGB(), 0.0F), 0.0F);
        tabGrammar.x = this.guiLeft + this.guiWidth - tabGrammar.width - tabBlock.width + 1;
        tabGrammar.y = this.guiTop - tabGrammar.height;
        tabGrammar = new GuiElementTab(tabGrammar.id, tabGrammar.x, tabGrammar.y, tabGrammar.validator,
                tabGrammar.listeners, tabGrammar.onLeftSide, tabGrammar.onRightSide,
                new GuiElementString(0, 0, 0, null, null, this.mc, grammarString, Color.BLACK.getRGB(), 0.0F), 0.0F);
        this.addElement(tabGrammar);

        String generalString = new TextComponentTranslation("gui.pages.block_writer.tab.general").getFormattedText();
        this.tabGeneral = new GuiElementTab(ElementIds.TAB_GENERAL.ordinal(), 0, 0, null, Arrays.asList(screenListener),
                false, false,
                new GuiElementString(0, 0, 0, null, null, this.mc, generalString, Color.BLACK.getRGB(), 0.0F), 0.0F);
        tabGeneral.x = this.guiLeft + this.guiWidth - tabGeneral.width - tabGrammar.width - tabBlock.width + 2;
        tabGeneral.y = this.guiTop - tabGeneral.height;
        tabGeneral = new GuiElementTab(tabGeneral.id, tabGeneral.x, tabGeneral.y, tabGeneral.validator,
                tabGeneral.listeners, tabGeneral.onLeftSide, tabGeneral.onRightSide,
                new GuiElementString(0, 0, 0, null, null, this.mc, generalString, Color.BLACK.getRGB(), 0.0F), 0.0F);
        this.addElement(tabGeneral);

        // Add elements to "General" tab.
        this.symbolIdField = tabGeneral
                .addElement(new GuiElementTextFieldLabeled(ElementIds.LABELED_FIELD_SYMBOL_ID.ordinal(),
                        this.guiLeft + 7, this.guiTop + 7, this.guiWidth - 14, null, null, mc,
                        new TextComponentTranslation("gui.pages.block_writer.element.symbol_id").getFormattedText(),
                        Color.BLACK.getRGB(),
                        "pages:block_" + this.blockState.getBlock().getRegistryName().getResourcePath().toString(),
                        Color.WHITE.getRGB(), 0.0F));

        this.poemEditor = tabGeneral.addElement(new GuiElementPoemEditor(ElementIds.POEM_EDITOR.ordinal(),
                this.guiLeft + 7, this.symbolIdField.y + this.symbolIdField.height + 8, this.guiWidth - 14, 54, null,
                null, mc, "Transform", "Constraint", "Block",
                Localization.getLocalizedBlockStateName(this.blockState).replace("Block", "").trim(), 0.0F));

        String cardRankString = new TextComponentTranslation("gui.pages.block_writer.element.card_rank")
                .getFormattedText();
        this.cardRankField = tabGeneral
                .addElement(new GuiElementTextFieldLabeled(ElementIds.LABELED_FIELD_CARD_RANK.ordinal(),
                        this.guiLeft + 7, this.poemEditor.y + this.poemEditor.height + 8,
                        this.fontRenderer.getStringWidth(cardRankString) + 4 + 24, KeyValidators::isInteger, null, mc,
                        cardRankString, Color.BLACK.getRGB(), "2", Color.WHITE.getRGB(), 0.0F));

        // Add elements to window (all tabs).
        String tooltipHelp = new TextComponentTranslation("gui.pages.block_writer.tooltips").getFormattedText();
        this.addElement(new GuiElementString(500,
                this.guiLeft + this.guiWidth - 7 - this.fontRenderer.getStringWidth(tooltipHelp),
                this.cardRankField.y + this.cardRankField.height + 14, null, null, this.mc, tooltipHelp,
                new Color(85, 85, 85).getRGB(), 0.0F));

        this.addElement(new GuiElementButton(ElementIds.BUTTON_SAVE.ordinal(), this.guiLeft + 7,
                cardRankField.y + cardRankField.height + 8, 64, 14, "Save", null, Arrays.asList(screenListener), 0.0F));

        // Add elements to "Grammar" tab.
        int id = 100;
        List<GuiElementListItemGrammarRule> categories = new ArrayList<GuiElementListItemGrammarRule>();
        for (BlockCategory category : this.blockCategories) {
            categories.add(new GuiElementListItemGrammarRule(id++, 0, 0, this.guiWidth - 14 - 14 - 4, null, null, mc,
                    category.getName().toString(), 0.0F));
        }
        this.grammarRuleEditor = tabGrammar
                .addElement(new GuiElementGrammarRuleEditor(ElementIds.GRAMMAR_EDITOR.ordinal(), this.guiLeft + 7,
                        this.guiTop + 8, this.guiWidth - 14, (18 * 4) + 2, null, null, this.mc, categories, 0.0F));

        // Add elements to "Block" tab.
        String baseString = new TextComponentTranslation("gui.pages.block_writer.element.instability_base")
                .getFormattedText();
        String exposedString = new TextComponentTranslation("gui.pages.block_writer.element.instability_exposed")
                .getFormattedText();
        int fieldWidth = (this.guiWidth - 14 - 8 - 4 - this.fontRenderer.getStringWidth(baseString + exposedString))
                / 2;

        baseInstabilityField = tabBlock.addElement(new GuiElementTextFieldLabeled(
                ElementIds.LABELED_FIELD_INSTABILITY_BASE.ordinal(), this.guiLeft + 7, this.guiTop + 7,
                this.fontRenderer.getStringWidth(baseString) + 2 + fieldWidth, KeyValidators::isFloat, null, this.mc,
                baseString, Color.BLACK.getRGB(), "", Color.WHITE.getRGB(), 0.0F));

        exposedInstabilityField = tabBlock
                .addElement(new GuiElementTextFieldLabeled(ElementIds.LABELED_FIELD_INSTABILITY_EXPOSED.ordinal(),
                        baseInstabilityField.x + baseInstabilityField.width + 8, baseInstabilityField.y,
                        this.fontRenderer.getStringWidth(exposedString) + 2 + fieldWidth, KeyValidators::isFloat, null,
                        this.mc, exposedString, Color.BLACK.getRGB(), "", Color.WHITE.getRGB(), 0.0F));

        Map<String, List<String>> propertiesMap = new HashMap<String, List<String>>();
        Collection<IProperty<?>> propertiesCollection = this.blockState.getPropertyKeys();
        for (IProperty<?> property : propertiesCollection) {
            Collection<?> possibleValues = property.getAllowedValues();
            List<String> acceptableValues = new ArrayList<String>();
            for (Object singleValue : possibleValues) {
                if (singleValue instanceof IStringSerializable) {
                    String valueName = ((IStringSerializable) singleValue).getName().trim().toLowerCase();
                    acceptableValues.add(valueName);
                } else {
                    acceptableValues.add(singleValue.toString().trim().toLowerCase());
                }
                propertiesMap.put(property.getName(), acceptableValues);
            }
        }
        propertiesEditor = tabBlock
                .addElement(new GuiElementBlockPropertiesEditor(ElementIds.PROPERTIES_EDITOR.ordinal(),
                        this.guiLeft + 7, exposedInstabilityField.y + exposedInstabilityField.height + 8,
                        this.guiWidth - 14, 64, null, null, this.mc, propertiesMap, 0.0F));

        tabGeneral.notifyListeners();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);

        if (isShiftKeyDown()) {

            for (GuiElement element : Iterables.concat(this.tabGeneral.elementList.values(),
                    this.tabGrammar.elementList.values(), this.tabBlock.elementList.values())) {

                String label = "";

                if (element.id == ElementIds.LABELED_FIELD_SYMBOL_ID.ordinal()) {
                    label = "symbol_id";
                } else if (element.id == ElementIds.POEM_EDITOR.ordinal()) {
                    label = "poem";
                } else if (element.id == ElementIds.LABELED_FIELD_CARD_RANK.ordinal()) {
                    label = "card_rank";
                } else if (element.id == ElementIds.GRAMMAR_EDITOR.ordinal()) {
                    label = "grammar_rules";
                } else if (element.id == ElementIds.LABELED_FIELD_INSTABILITY_BASE.ordinal()) {
                    label = "instability_base";
                } else if (element.id == ElementIds.LABELED_FIELD_INSTABILITY_EXPOSED.ordinal()) {
                    label = "instability_exposed";
                } else if (element.id == ElementIds.PROPERTIES_EDITOR.ordinal()) {
                    label = "properties";
                }

                if (!label.trim().isEmpty()) {
                    this.drawTranslatedTooltip(element, label, mouseX, mouseY);
                }

            }

        }

    }

    public void drawTranslatedTooltip(GuiElement element, String path, int mouseX, int mouseY) {
        if (element.isMouseOver(mouseX, mouseY)) {
            this.drawHoveringText(new TextComponentTranslation("tooltip." + Pages.MOD_ID + ".block_writer." + path)
                    .getUnformattedText(), mouseX, mouseY);
        }
    }

    public static enum ElementIds {
        WINDOW,
        SCREEN_LISTENER,
        TAB_BLOCK,
        TAB_GRAMMAR,
        TAB_GENERAL,
        LABELED_FIELD_SYMBOL_ID,
        POEM_EDITOR,
        LABELED_FIELD_CARD_RANK,
        GRAMMAR_EDITOR,
        LABELED_FIELD_INSTABILITY_BASE,
        LABELED_FIELD_INSTABILITY_EXPOSED,
        PROPERTIES_EDITOR,
        BUTTON_SAVE;
    }

}
