package pages.client.gui;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import pages.client.gui.element.generic.GuiElement;

public class GuiScreenBase extends GuiScreen {

    public Map<Integer, GuiElement> elementList = new HashMap<Integer, GuiElement>();
    public static GuiElement screenElement;

    @Override
    public void initGui() {
        screenElement = new GuiElement(Integer.MIN_VALUE, 0, 0, this.width, this.height, null, null, 0.0F);
    }

    public <T extends GuiElement> T addElement(T element) {
        this.elementList.put(element.id, element);
        return element;
    }

    /**
     * Draws the GUI screen and all child elements. If overriding, call the super
     * method to make sure all child elements get drawn.
     */
    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        for (GuiElement element : this.elementList.values()) {
            element.drawElement(this.mc, mouseX, mouseY, partialTicks);
            element.drawChildElements(this.mc, mouseX, mouseY, partialTicks);
        }
    }

    /**
     * Handles keyboard input. If overriding, call the super method to pass the
     * keyboard input to all child elements.
     */
    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        super.keyTyped(typedChar, keyCode);
        for (GuiElement element : this.elementList.values()) {
            element.keyTyped(typedChar, keyCode);
        }
    }

    /**
     * Handles mouse clicks. If overriding, call the super method to pass the mouse
     * click to all child elements.
     */
    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        for (GuiElement element : this.elementList.values()) {
            element.mousePressed(this.mc, mouseX, mouseY, mouseButton);
        }
    }

    /**
     * Handles mouse button releases. If overriding, call the super method to pass
     * the mouse button release to all child elements.
     */
    @Override
    protected void mouseReleased(int mouseX, int mouseY, int mouseButton) {
        super.mouseReleased(mouseX, mouseY, mouseButton);
        for (GuiElement element : this.elementList.values()) {
            element.mouseReleased(mouseX, mouseY, mouseButton);
        }
    }

    /**
     * Handles mouse dragging. If overriding, call the super method to pass the
     * mouse drag to all child elements.
     */
    @Override
    protected void mouseClickMove(int mouseX, int mouseY, int mouseButton, long timeSinceLastClick) {
        super.mouseClickMove(mouseX, mouseY, mouseButton, timeSinceLastClick);
        for (GuiElement element : this.elementList.values()) {
            element.mouseDragged(mc, mouseX, mouseY, mouseButton);
        }
    }

    @Override
    public void handleMouseInput() throws IOException {
        super.handleMouseInput();
        int x = Mouse.getEventX() * this.width / this.mc.displayWidth;
        int y = this.height - Mouse.getEventY() * this.height / this.mc.displayHeight - 1;
        int i = Integer.signum(Mouse.getEventDWheel());
        for (GuiElement element : this.elementList.values()) {
            element.onMouseWheel(x, y, i);
        }
    }

    @Override
    public void setWorldAndResolution(Minecraft mc, int width, int height) {
        Map<Integer, GuiElement> backupList = new HashMap<Integer, GuiElement>(this.elementList);
        this.elementList.clear();
        super.setWorldAndResolution(mc, width, height);
        for (GuiElement element : this.elementList.values()) {
            if (backupList.get(element.id) != null) {
                element.restore(backupList.get(element.id));
                element.restoreChildren(backupList.get(element.id));
            }
        }
    }

    public void elementChanged(GuiElement element) {
    }

    @Override
    public void onGuiClosed() {
        Keyboard.enableRepeatEvents(false);
        GuiElement.setMouseCaptured(false);
    }

}
