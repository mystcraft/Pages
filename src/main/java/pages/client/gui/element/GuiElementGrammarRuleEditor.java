package pages.client.gui.element;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.client.Minecraft;
import net.minecraft.util.text.TextComponentTranslation;
import pages.client.gui.GuiScreenBase;
import pages.client.gui.element.generic.GuiElement;
import pages.client.gui.element.generic.GuiElementScrollableList;
import pages.client.gui.element.generic.GuiElementString;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementGrammarRuleEditor extends GuiElement {

    public GuiElementScrollableList list;

    public GuiElementGrammarRuleEditor(int id, int x, int y, int width, int height, IKeyValidator validator,
            List<GuiElement> listeners, Minecraft mc, List<? extends GuiElement> items, float zLevel) {
        super(id, x, y, width, height, validator, listeners, zLevel);
        this.addElement(new GuiElementString(0, x, y, validator, listeners, mc,
                new TextComponentTranslation("gui.pages.block_writer.element.grammar").getFormattedText(), COLOR_BLACK,
                zLevel));
        this.list = this.addElement(new GuiElementScrollableList(1, x, y + 10, width, height, validator, listeners, 2,
                mc, items, Arrays.asList(GuiScreenBase.screenElement), 4, zLevel));
    }

    public Map<String, Integer> getEnabledGrammarRules() {
        Map<String, Integer> grammarMap = new HashMap<String, Integer>();
        for (GuiElement element : this.list.getListedItems()) {
            if (element instanceof GuiElementListItemGrammarRule) {
                GuiElementListItemGrammarRule grammarRuleItem = (GuiElementListItemGrammarRule) element;
                if (grammarRuleItem.checkbox.isChecked) {
                    grammarMap.put(grammarRuleItem.title.value,
                            grammarRuleItem.textfield.getText().trim().isEmpty() ? null
                                    : Integer.valueOf(grammarRuleItem.textfield.getText()));
                }
            }
        }
        return grammarMap;
    }

}
