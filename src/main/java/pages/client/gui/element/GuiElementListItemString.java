package pages.client.gui.element;

import java.util.List;

import net.minecraft.client.Minecraft;
import pages.client.gui.element.generic.GuiElement;
import pages.client.gui.element.generic.GuiElementStringTruncated;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementListItemString extends GuiElement {

    public GuiElementStringTruncated string;

    public GuiElementListItemString(int id, int x, int y, int width, IKeyValidator validator,
            List<GuiElement> listeners, Minecraft mc, String value, int color, float zLevel) {
        super(id, x, y, width, 14, validator, listeners, zLevel);
        this.string = this.addElement(new GuiElementStringTruncated(0, x + 3, y + 3, width - 6, validator, listeners,
                mc, value, color, zLevel + 1.0F));
    }

    @Override
    public void drawElement(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        if (this.isVisible()) {
            drawRect(this.x, this.y, this.x + this.width - 1, this.y + this.height - 1, COLOR_WHITE, this.zLevel);
            drawRect(this.x + 1, this.y + 1, this.x + this.width, this.y + this.height, COLOR_DARKER_GRAY, this.zLevel);
            drawRect(this.x + 1, this.y + 1, this.x + this.width - 1, this.y + this.height - 1, COLOR_LIGHT_GRAY,
                    this.zLevel);
            drawPoint(this.x, this.y + this.height - 1, COLOR_LIGHT_GRAY, this.zLevel);
            drawPoint(this.x + this.width - 1, this.y, COLOR_LIGHT_GRAY, this.zLevel);
        }
    }

    @Override
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY, int mouseButton) {
        boolean used = false;
        if (this.isMouseOver(mouseX, mouseY) && this.isVisible()) {
            this.notifyListeners();
            used = true;
        }
        return used == true ? true : super.mousePressed(mc, mouseX, mouseY, mouseButton);
    }

}
