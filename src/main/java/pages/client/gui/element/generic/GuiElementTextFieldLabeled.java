package pages.client.gui.element.generic;

import java.util.List;

import net.minecraft.client.Minecraft;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementTextFieldLabeled extends GuiElement {

    public GuiElementTextField textFieldElement;

    public GuiElementTextFieldLabeled(int id, int x, int y, int width, IKeyValidator validator,
            List<GuiElement> listeners, Minecraft mc, String label, int labelColor, String fieldText,
            int fieldTextColor, float zLevel) {
        super(id, x, y, width, 12, validator, listeners, zLevel);
        int labelWidth = mc.fontRenderer.getStringWidth(label);
        this.addElement(new GuiElementString(0, x, y + 2, validator, listeners, mc, label, labelColor, zLevel));
        this.textFieldElement = this.addElement(new GuiElementTextField(1, x + labelWidth + 4, y,
                width - labelWidth - 4, validator, listeners, mc, fieldTextColor, fieldText, zLevel + 1.0F));
    }

    public String getText() {
        return this.textFieldElement.textField.getText();
    }

}
