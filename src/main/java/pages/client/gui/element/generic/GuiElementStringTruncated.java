package pages.client.gui.element.generic;

import java.util.List;

import net.minecraft.client.Minecraft;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementStringTruncated extends GuiElementString {

    public GuiElementStringTruncated(int id, int x, int y, int width, IKeyValidator validator,
            List<GuiElement> listeners, Minecraft mc, String text, int color, float zLevel) {
        super(id, x, y, validator, listeners, mc, text, color, zLevel);
        int stringWidth = mc.fontRenderer.getStringWidth(text);
        String ellipsis = "...";
        boolean edited = false;
        while (stringWidth > width) {
            text = text.substring(0, text.length() - 1);
            edited = true;
            stringWidth = mc.fontRenderer.getStringWidth(text + ellipsis);
        }
        if (edited) {
            text += ellipsis;
        }
        this.value = text;
    }

}
