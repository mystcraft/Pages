package pages.client.gui.element.generic.keyvalidator;

/**
 * A functional interface that is not meant to be explicitly implemented. See
 * {@link pages.client.gui.element.generic.keyvalidator.KeyValidators} for how
 * to implicitly implement this interface and
 * {@link pages.client.gui.element.generic.GuiElementTextField#keyTyped(char, int)}
 * for how to utilize it.
 */
@FunctionalInterface
public interface IKeyValidator {
    public boolean isValid(char typedChar, int keyCode, String existingString);
}