package pages.client.gui.element.generic;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;
import pages.client.gui.element.generic.keyvalidator.KeyValidators;

/**
 * Meant to be used as a base for parts of a GuiScreen. Similar to vanilla's
 * GuiButton but with more generic naming. Supports z-level logic.
 */
@SideOnly(Side.CLIENT)
public class GuiElement extends Gui {

    public static final int COLOR_BLACK = Color.BLACK.getRGB();
    public static final int COLOR_DARKER_GRAY = new Color(55, 55, 55).getRGB();
    public static final int COLOR_DARK_GRAY = new Color(85, 85, 85).getRGB();
    public static final int COLOR_GRAY = new Color(139, 139, 139).getRGB();
    public static final int COLOR_LIGHT_GRAY = new Color(198, 198, 198).getRGB();
    public static final int COLOR_WHITE = Color.WHITE.getRGB();

    public int id;
    public int x;
    public int y;
    public int width;
    public int height;
    protected boolean visible;
    protected boolean enabled;
    public Map<Integer, GuiElement> elementList = new HashMap<Integer, GuiElement>();

    protected static boolean mouseCaptured = false;
    protected static List<GuiElement> capturedElements = new ArrayList<GuiElement>();

    public IKeyValidator validator;
    public List<GuiElement> listeners;

    public GuiElement(int id, int x, int y, int width, int height, @Nullable IKeyValidator validator,
            List<GuiElement> listeners, float zLevel) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.setZLevel(zLevel);
        this.setVisible(true);
        this.setEnabled(true);
        this.validator = validator == null ? KeyValidators::isAnything : validator;
        this.listeners = listeners == null ? new ArrayList<GuiElement>() : listeners;
    }

    public <T extends GuiElement> T addElement(T element) {
        this.elementList.put(element.id, element);
        return element;
    }

    /**
     * A zLevel-sensitive version of drawRect() in the Gui class.
     */
    public static void drawRect(int left, int top, int right, int bottom, int color, float zLevel) {
        GlStateManager.pushMatrix();
        GlStateManager.translate(0.0F, 0.0F, zLevel);
        drawRect(left, top, right, bottom, color);
        GlStateManager.popMatrix();
    }

    public void setCoords(int x, int y) {
        int dx = x - this.x;
        int dy = y - this.y;
        this.x += dx;
        this.y += dy;
        for (GuiElement element : this.elementList.values()) {
            element.setCoords(element.x + dx, element.y + dy);
        }
    }

    /**
     * Draws this element to the screen.
     */
    public void drawElement(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
    }

    /**
     * Draws all children of this element.
     */
    public final void drawChildElements(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        if (this.isVisible()) {
            for (GuiElement element : this.elementList.values()) {
                element.drawElement(mc, mouseX, mouseY, partialTicks);
                element.drawChildElements(mc, mouseX, mouseY, partialTicks);
            }
        }
    }

    /**
     * Handles keyboard input. If overriding, call the super method to pass the
     * keyboard input to all child elements.
     */
    public void keyTyped(char typedChar, int keyCode) {
        for (GuiElement element : this.elementList.values()) {
            element.keyTyped(typedChar, keyCode);
        }
    }

    /**
     * Handles mouse clicks. If overriding, call the super method to pass the mouse
     * click to all child elements.
     * 
     * @param mc
     * @param mouseX
     * @param mouseY
     * @param mouseButton
     * @return Whether the mouse click has been used up. Return <b>true</b> if you
     *         don't want the click to apply to any other elements. Otherwise,
     *         return <b>false</b>.
     */
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY, int mouseButton) {
        boolean used = false;
        for (GuiElement element : this.elementList.values()) {
            used = used == true ? true : element.mousePressed(mc, mouseX, mouseY, mouseButton);
        }
        return used;
    }

    /**
     * Handles mouse dragging. If overriding, call the super method to pass the
     * mouse drag to all child elements.
     */
    public void mouseDragged(Minecraft mc, int mouseX, int mouseY, int mouseButton) {
        for (GuiElement element : this.elementList.values()) {
            element.mouseDragged(mc, mouseX, mouseY, mouseButton);
        }
    }

    /**
     * Handles mouse button releases. If overriding, call the super method to pass
     * the mouse button release to all child elements.
     */
    public void mouseReleased(int mouseX, int mouseY, int mouseButton) {
        for (GuiElement element : this.elementList.values()) {
            element.mouseReleased(mouseX, mouseY, mouseButton);
        }
    }

    public void onMouseWheel(int mouseX, int mouseY, int value) {
        for (GuiElement element : this.elementList.values()) {
            element.onMouseWheel(mouseX, mouseY, value);
        }
    }

    /**
     * Whether the mouse cursor is currently over this element.
     */
    public boolean isMouseOver(int mouseX, int mouseY) {
        if (mouseCaptured && !capturedElements.contains(this) || !this.isVisible()) {
            return false;
        }
        return mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
    }

    public void drawCenteredString(FontRenderer fontRendererIn, String text, int x, int y, int color, float zLevel) {
        GlStateManager.pushMatrix();
        GlStateManager.translate(0.0F, 0.0F, zLevel);
        fontRendererIn.drawString(text, x - fontRendererIn.getStringWidth(text) / 2, y, color);
        GlStateManager.popMatrix();
    }

    public void drawString(FontRenderer fontRendererIn, String text, int x, int y, int color, boolean shadow,
            float zLevel) {
        GlStateManager.pushMatrix();
        GlStateManager.translate(0.0F, 0.0F, zLevel);
        fontRendererIn.drawString(text, x, y, color, shadow);
        GlStateManager.popMatrix();
    }

    /**
     * Used to restore important information when the main Gui is resized, like the
     * text in a text field. You should probably not restore base elements like the
     * x and y coordinates because the new values are correct.
     */
    public void restore(GuiElement backup) {
    }

    public final void restoreChildren(GuiElement backup) {
        for (GuiElement element : this.elementList.values()) {
            element.restore(backup.elementList.get(element.id));
            element.restoreChildren(backup.elementList.get(element.id));
        }
    }

    public static void drawPoint(int left, int top, int color, float zLevel) {
        drawRect(left, top, left + 1, top + 1, color, zLevel);
    }

    protected void drawHorizontalLine(int startX, int endX, int y, int color, float zLevel) {
        if (endX < startX) {
            int i = startX;
            startX = endX;
            endX = i;
        }

        drawRect(startX, y, endX + 1, y + 1, color, zLevel);
    }

    protected void drawVerticalLine(int x, int startY, int endY, int color, float zLevel) {
        if (endY < startY) {
            int i = startY;
            startY = endY;
            endY = i;
        }

        drawRect(x, startY + 1, x + 1, endY, color, zLevel);
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
        for (GuiElement element : this.elementList.values()) {
            element.setVisible(visible);
        }
    }

    public boolean isVisible() {
        return this.visible;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        for (GuiElement element : this.elementList.values()) {
            element.setEnabled(enabled);
        }
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void drawTruncatedString(FontRenderer fontRenderer, int x, int y, int width, String text, int color,
            float zLevel) {
        int stringWidth = fontRenderer.getStringWidth(text);
        String ellipsis = "...";
        boolean edited = false;
        while (stringWidth > width) {
            text = text.substring(0, text.length() - 1);
            edited = true;
            stringWidth = fontRenderer.getStringWidth(text + ellipsis);
        }
        if (edited) {
            text += ellipsis;
        }
        GlStateManager.pushMatrix();
        GlStateManager.translate(0.0F, 0.0F, zLevel);
        fontRenderer.drawString(text, x, y, color);
        GlStateManager.popMatrix();
    }

    public void setZLevel(float level) {
        this.zLevel = level;
    }

    public static void setMouseCaptured(boolean captured) {
        setMouseCaptured(captured, new GuiElement[0]);
    }

    public static void setMouseCaptured(boolean captured, GuiElement... elements) {
        mouseCaptured = captured;
        capturedElements = new ArrayList<GuiElement>();
        for (GuiElement element : elements) {
            element.capture();
        }
    }

    public void capture() {
        capturedElements.add(this);
        for (GuiElement element : this.elementList.values()) {
            element.capture();
        }
    }

    public void release() {
        capturedElements.remove(this);
        for (GuiElement element : this.elementList.values()) {
            element.release();
        }
    }

    public void notifyListeners() {
        for (GuiElement element : this.listeners) {
            element.elementChanged(this);
        }
    }

    public void elementChanged(GuiElement element) {
    }

}
