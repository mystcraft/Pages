package pages.client.gui.element.generic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.util.math.MathHelper;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementScrollableList extends GuiElement {

    public GuiElementScrollbarVertical scrollbar;
    protected int maxItems;
    protected List<GuiElement> allItems = new ArrayList<GuiElement>();
    protected List<GuiElement> displayedItems = new ArrayList<GuiElement>();
    protected List<? extends GuiElement> validScrollWheelElements = new ArrayList<GuiElement>();

    public GuiElementScrollableList(int id, int x, int y, int width, int height, IKeyValidator validator,
            List<GuiElement> listeners, int scrollbarGap, Minecraft mc, List<? extends GuiElement> items,
            List<GuiElement> validScrollWheelElements, int maxItems, float zLevel) {
        super(id, x, y, width, height, validator, listeners, zLevel);
        this.maxItems = maxItems;
        this.validScrollWheelElements = validScrollWheelElements;
        int localId = 0;
        this.scrollbar = this.addElement(new GuiElementScrollbarVertical(localId++, x + width - 16 + scrollbarGap, y,
                height, validator, Arrays.asList(this), validScrollWheelElements, zLevel));
        for (GuiElement element : items) {
            this.allItems.add(this.addElement(element));
        }
        this.displayedItems = new ArrayList<GuiElement>(
                this.allItems.subList(0, MathHelper.clamp(this.allItems.size(), 0, maxItems)));
        this.refineDisplayedItems();
    }

    protected void refineDisplayedItems() {
        if (this.displayedItems != null) {
            for (GuiElement item : this.allItems) {
                item.setVisible(false);
                item.setCoords(0, 0);
            }
            int currentY = this.y + 1;
            for (GuiElement item : this.displayedItems) {
                item.setVisible(true);
                item.setCoords(this.x + 1, currentY);
                currentY += item.height;
            }
            if (this.allItems.size() <= this.maxItems) {
                this.scrollbar.setEnabled(false);
                this.scrollbar.setVisible(false);
            }
        }
    }

    @Override
    public void drawElement(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        if (this.isVisible()) {
            drawRect(this.x, this.y, this.x + this.width - this.scrollbar.width - 3, this.y + this.height - 1,
                    COLOR_DARKER_GRAY, this.zLevel);
            drawRect(this.x + 1, this.y + 1, this.x + this.width - this.scrollbar.width - 2, this.y + this.height,
                    COLOR_WHITE, this.zLevel);
            drawRect(this.x + 1, this.y + 1, this.x + this.width - this.scrollbar.width - 3, this.y + this.height - 1,
                    COLOR_GRAY, this.zLevel);
            drawPoint(this.x + this.width - this.scrollbar.width - 3, this.y, COLOR_GRAY, this.zLevel);
            drawPoint(this.x, this.y + this.height - 1, COLOR_GRAY, this.zLevel);
        }
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (this.allItems != null && this.scrollbar != null) {
            if (this.allItems.size() <= this.maxItems) {
                this.scrollbar.setEnabled(false);
                this.scrollbar.setVisible(false);
            }
        }
    }

    @Override
    public void elementChanged(GuiElement element) {
        if (element instanceof GuiElementScrollbarVertical) {
            GuiElementScrollbarVertical scrollbarElement = (GuiElementScrollbarVertical) element;
            for (int i = 0, firstDisplayedItem = MathHelper.clamp(
                    Math.round((this.allItems.size() - maxItems) * scrollbarElement.getPuckPosition()), 0,
                    this.allItems.size() - maxItems); i < this.displayedItems.size()
                            && firstDisplayedItem >= 0; i++, firstDisplayedItem++) {
                this.displayedItems.set(i, this.allItems.get(firstDisplayedItem));
            }
            this.refineDisplayedItems();
        }
    }

    public List<GuiElement> getListedItems() {
        return this.allItems;
    }

}
