package pages.client.gui.element.generic;

import java.util.List;

import net.minecraft.client.Minecraft;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementButtonDropdown extends GuiElementButton {

    public boolean isOpen = false;

    public GuiElementButtonDropdown(int id, int x, int y, int width, int height, String label, IKeyValidator validator,
            List<GuiElement> listeners, float zLevel) {
        super(id, x, y, width, height, label, validator, listeners, zLevel);
    }

    @Override
    public void drawElement(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        if (this.isVisible()) {
            int highlightColor = COLOR_WHITE;
            int shadowColor = COLOR_DARK_GRAY;
            int mainColor = COLOR_LIGHT_GRAY;
            int textColor = COLOR_BLACK;
            int cornerColor = COLOR_GRAY;
            if (this.isMouseOver(mouseX, mouseY) || this.isOpen) {
                shadowColor = COLOR_DARKER_GRAY;
                mainColor = COLOR_GRAY;
                textColor = COLOR_LIGHT_GRAY;
                cornerColor = COLOR_DARK_GRAY;
            }

            // Draw button.
            drawRect(this.x, this.y, this.x + this.width - 1, this.y + this.height - 1, highlightColor, this.zLevel);
            drawRect(this.x + 1, this.y + 1, this.x + this.width, this.y + this.height, shadowColor, this.zLevel);
            drawRect(this.x + 1, this.y + 1, this.x + this.width - 1, this.y + this.height - 1, mainColor, this.zLevel);
            drawPoint(this.x, this.y + this.height - 1, cornerColor, this.zLevel);
            drawPoint(this.x + this.width - 1, this.y, cornerColor, this.zLevel);

            // Draw label.
            this.drawTruncatedString(mc.fontRenderer, this.x + 3, this.y + 3, this.width - 9 - 7, this.label, textColor,
                    this.zLevel);

            // Draw arrow.
            this.drawHorizontalLine(this.x + this.width - 10, this.x + this.width - 4, this.y + 5, textColor,
                    this.zLevel);
            this.drawHorizontalLine(this.x + this.width - 9, this.x + this.width - 5, this.y + 6, textColor,
                    this.zLevel);
            this.drawHorizontalLine(this.x + this.width - 8, this.x + this.width - 6, this.y + 7, textColor,
                    this.zLevel);
            this.drawHorizontalLine(this.x + this.width - 7, this.x + this.width - 7, this.y + 8, textColor,
                    this.zLevel);
        }
    }

    @Override
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY, int mouseButton) {
        boolean used = false;
        if (this.isMouseOver(mouseX, mouseY) && this.isVisible() && this.isEnabled()) {
            this.isOpen = !this.isOpen;
            used = true;
            this.notifyListeners();
        }
        return used == true ? true : super.mousePressed(mc, mouseX, mouseY, mouseButton);
    }

    @Override
    public void restore(GuiElement backup) {
        if (backup instanceof GuiElementButtonDropdown) {
            this.isOpen = ((GuiElementButtonDropdown) backup).isOpen;
        }
    }

}
