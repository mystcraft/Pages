package pages.client.gui.element.generic;

import java.util.List;

import net.minecraft.client.Minecraft;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementString extends GuiElement {

    public String value;
    public int color;

    public GuiElementString(int id, int x, int y, IKeyValidator validator, List<GuiElement> listeners, Minecraft mc,
            String text, int color, float zLevel) {
        this(id, x, y, mc.fontRenderer.getStringWidth(text), mc.fontRenderer.FONT_HEIGHT - 1, validator, listeners,
                zLevel);
        this.value = text;
        this.color = color;
    }

    private GuiElementString(int id, int x, int y, int width, int height, IKeyValidator validator,
            List<GuiElement> listeners, float zLevel) {
        super(id, x, y, width, height, validator, listeners, zLevel);
    }

    @Override
    public void drawElement(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        if (this.isVisible()) {
            this.drawString(mc.fontRenderer, this.value, this.x, this.y, this.color, false, this.zLevel);
        }
    }

}
