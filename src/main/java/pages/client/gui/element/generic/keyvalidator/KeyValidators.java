package pages.client.gui.element.generic.keyvalidator;

import java.awt.event.KeyEvent;

/**
 * Some standard implementations of IKeyValidator to validate keypresses. For
 * example, you might want to limit a text field to a certain type of input.
 */
public class KeyValidators {

    public static boolean isAnything(char typedChar, int keyCode, String existingString) {
        return true;
    }

    public static boolean isAlphabetic(char typedChar, int keyCode, String existingString) {
        return Character.isAlphabetic(keyCode) || typedChar == KeyEvent.VK_BACK_SPACE;
    }

    public static boolean isInteger(char typedChar, int keyCode, String existingString) {
        return Character.isDigit(typedChar) || typedChar == KeyEvent.VK_BACK_SPACE;
    }

    public static boolean isFloat(char typedChar, int keyCode, String existingString) {
        return (Character.isDigit(typedChar) || (typedChar == KeyEvent.VK_PERIOD && !existingString.contains(".")))
                || typedChar == KeyEvent.VK_BACK_SPACE;
    }
}
