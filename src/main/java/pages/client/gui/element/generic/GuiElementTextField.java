package pages.client.gui.element.generic;

import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementTextField extends GuiElement {

    public GuiTextField textField;

    public GuiElementTextField(int id, int x, int y, int width, IKeyValidator validator, List<GuiElement> listeners,
            Minecraft mc, int color, String value, float zLevel) {
        this(id, x, y, width, validator, listeners, mc, color, zLevel);
        this.textField.setText(value);
    }

    public GuiElementTextField(int id, int x, int y, int width, IKeyValidator validator, List<GuiElement> listeners,
            Minecraft mc, int color, float zLevel) {
        super(id, x, y, width, 12, validator, listeners, zLevel);
        this.textField = new GuiTextField(id, mc.fontRenderer, x + 2, y + 2, width - 4,
                mc.fontRenderer.FONT_HEIGHT + 1);
        this.textField.setEnableBackgroundDrawing(false);
        this.textField.setMaxStringLength(64);
        this.textField.setTextColor(color);
    }

    @Override
    public void drawElement(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        if (this.isVisible()) {
            this.drawHorizontalLine(this.x, this.x + this.width - 1, this.y, COLOR_DARKER_GRAY, this.zLevel);
            this.drawVerticalLine(this.x, this.y, this.y + this.height - 1, COLOR_DARKER_GRAY, this.zLevel);
            drawRect(this.x + 1, this.y + 1, this.x + this.width - 1, this.y + this.height - 1, COLOR_GRAY,
                    this.zLevel);
            drawPoint(this.x, this.y + this.height - 1, COLOR_GRAY, this.zLevel);
            drawPoint(this.x + this.width - 1, this.y, COLOR_GRAY, this.zLevel);
            this.drawVerticalLine(this.x + this.width - 1, this.y, this.y + this.height, COLOR_WHITE, this.zLevel);
            this.drawHorizontalLine(this.x + 1, this.x + this.width - 1, this.y + this.height - 1, COLOR_WHITE,
                    this.zLevel);
            GlStateManager.pushMatrix();
            GlStateManager.translate(0.0F, 0.0F, 1.0F);
            this.textField.drawTextBox();
            GlStateManager.popMatrix();
        }
    }

    @Override
    public void keyTyped(char typedChar, int keyCode) {
        if (this.isVisible() && this.validator.isValid(typedChar, keyCode, this.textField.getText())) {
            this.textField.textboxKeyTyped(typedChar, keyCode);
            this.notifyListeners();
        }
    }

    @Override
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY, int mouseButton) {
        if (this.isVisible() && this.isEnabled()) {
            this.textField.mouseClicked(mouseX, mouseY, mouseButton);
        }
        return false;
    }

    @Override
    public void restore(GuiElement backup) {
        if (backup instanceof GuiElementTextField) {
            this.textField.setText(((GuiElementTextField) backup).textField.getText());
        }
    }

    @Override
    public void setCoords(int x, int y) {
        int dx = x - this.x;
        int dy = y - this.y;
        this.x += dx;
        this.y += dy;
        for (GuiElement element : this.elementList.values()) {
            element.setCoords(element.x + dx, element.y + dy);
        }
        this.textField.x += dx;
        this.textField.y += dy;
    }

    public String getText() {
        return this.textField.getText();
    }

}
