package pages.client.gui.element.generic;

import java.util.List;

import net.minecraft.client.Minecraft;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementCheckBox extends GuiElement {

    public boolean isChecked;

    public GuiElementCheckBox(int id, int x, int y, int width, int height, IKeyValidator validator,
            List<GuiElement> listeners, float zLevel, boolean checked) {
        super(id, x, y, width, height, validator, listeners, zLevel);
        this.isChecked = checked;
    }

    public GuiElementCheckBox(int id, int x, int y, int width, int height, IKeyValidator validator,
            List<GuiElement> listeners, float zLevel) {
        this(id, x, y, width, height, validator, listeners, zLevel, false);
    }

    @Override
    public void drawElement(Minecraft mc, int mouseX, int mouseY, float partialTicks) {

        // Draw box.
        drawRect(this.x, this.y, this.x + this.width - 1, this.y + this.height - 1, COLOR_DARKER_GRAY, this.zLevel);
        drawRect(this.x + 1, this.y + 1, this.x + this.width, this.y + this.height, COLOR_WHITE, this.zLevel);
        drawRect(this.x + 1, this.y + 1, this.x + this.width - 1, this.y + this.height - 1, COLOR_GRAY, this.zLevel);
        drawPoint(this.x + this.width - 1, this.y, COLOR_GRAY, this.zLevel);
        drawPoint(this.x, this.y + this.height - 1, COLOR_GRAY, this.zLevel);

        // Draw x if checked.
        if (this.isChecked) {
            int xSize = 6;
            this.drawString(mc.fontRenderer, "x", this.x + ((this.width - xSize) / 2),
                    this.y + ((this.height - xSize) / 2) - 2, COLOR_WHITE, true, this.zLevel);
        }

        // Draw child elements.
        super.drawElement(mc, mouseX, mouseY, partialTicks);
    }

    @Override
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY, int mouseButton) {
        boolean used = super.mousePressed(mc, mouseX, mouseY, mouseButton);
        if (this.isMouseOver(mouseX, mouseY) && this.isVisible() && this.enabled) {
            this.isChecked = used == true ? this.isChecked : !this.isChecked;
        }
        return used;
    }

    @Override
    public void restore(GuiElement backup) {
        if (backup instanceof GuiElementCheckBox) {
            this.isChecked = ((GuiElementCheckBox) backup).isChecked;
        }
    }

}
