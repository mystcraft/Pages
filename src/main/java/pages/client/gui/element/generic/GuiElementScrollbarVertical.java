package pages.client.gui.element.generic;

import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementScrollbarVertical extends GuiElement {

    protected static final ResourceLocation TEXTURE = new ResourceLocation("minecraft",
            "textures/gui/container/creative_inventory/tabs.png");

    protected int textureLightX = 232;
    protected int textureDarkX = 244;
    protected int textureY = 0;
    protected int puckWidth = 12;
    protected int puckHeight = 15;

    protected int puckX;
    protected int puckY;
    protected int practicalHeight;
    protected int localY;

    protected List<GuiElement> validScrollElements;
    protected boolean mouseDown;

    public GuiElementScrollbarVertical(int id, int x, int y, int height, IKeyValidator validator, List<GuiElement> listeners,
            List<GuiElement> validScrollElements, float zLevel) {
        super(id, x, y, 14, height, validator, listeners, zLevel);
        this.puckX = x + 1;
        this.puckY = y + 1;
        this.practicalHeight = height - 2 - this.puckHeight;
        this.mouseDown = false;
        this.validScrollElements = validScrollElements;
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
    }

    @Override
    public void drawElement(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        if (this.isVisible()) {
            this.drawHorizontalLine(this.x, this.x + this.width - 2, this.y, COLOR_DARKER_GRAY);
            this.drawVerticalLine(this.x, this.y, this.y + this.height - 1, COLOR_DARKER_GRAY);
            drawRect(this.x + 1, this.y + 1, this.x + this.width - 1, this.y + this.height - 1, COLOR_GRAY,
                    this.zLevel);
            drawRect(this.x, this.y + this.height - 1, this.x + 1, this.y + this.height, COLOR_GRAY, this.zLevel);
            drawRect(this.x + this.width - 1, this.y, this.x + this.width, this.y + 1, COLOR_GRAY, this.zLevel);
            this.drawVerticalLine(this.x + this.width - 1, this.y, this.y + this.height - 1, COLOR_WHITE);
            this.drawHorizontalLine(this.x + 1, this.x + this.width - 1, this.y + this.height - 1, COLOR_WHITE);
            mc.getTextureManager().bindTexture(TEXTURE);
            this.drawTexturedModalRect(this.puckX, this.puckY, this.textureLightX, this.textureY, this.puckWidth,
                    this.puckHeight);
        }
    }

    @Override
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY, int mouseButton) {
        if (this.isVisible()) {
            if (mouseButton == 0) {
                if (this.isOverPuck(mouseX, mouseY)) {
                    this.localY = mouseY - this.puckY;
                    this.mouseDown = true;
                    return true;
                }
            }
        }
        return false;

    }

    @Override
    public void onMouseWheel(int mouseX, int mouseY, int value) {
        if (this.isVisible() && this.isEnabled()) {
            boolean valid = false;
            for (GuiElement element : this.validScrollElements) {
                valid = element.isMouseOver(mouseX, mouseY);
                if (valid == true)
                    break;
            }
            if (valid == true) {
                this.setPuckPosition(MathHelper.clamp(this.getPuckPosition() - (value * 0.1F), 0.0F, 1.0F));
                this.notifyListeners();
            }
        }
        super.onMouseWheel(mouseX, mouseY, value);
    }

    @Override
    public void mouseDragged(Minecraft mc, int mouseX, int mouseY, int mouseButton) {
        if (this.isVisible()) {
            if (mouseButton == 0) {
                if (this.mouseDown) {
                    this.puckY = MathHelper.clamp(mouseY - this.localY, this.y + 1,
                            this.y + this.height - this.puckHeight - 1);
                    this.notifyListeners();
                }
            }
            super.mouseDragged(mc, mouseX, mouseY, mouseButton);
        }
    }

    @Override
    public void mouseReleased(int mouseX, int mouseY, int mouseButton) {
        this.mouseDown = false;
        super.mouseReleased(mouseX, mouseY, mouseButton);
    }

    @Override
    public void restore(GuiElement backup) {
        if (backup instanceof GuiElementScrollbarVertical) {
            this.setPuckPosition(((GuiElementScrollbarVertical) backup).getPuckPosition());
            this.notifyListeners();
        }
    }

    public boolean isOverPuck(int x, int y) {
        if (x >= this.puckX && x < this.puckX + this.puckWidth && y >= this.puckY && y < this.puckY + this.puckHeight) {
            return true;
        }
        return false;
    }

    public void setPuckPosition(float pos) {
        pos = MathHelper.clamp(pos, 0.0F, 1.0F);
        this.puckY = (int) ((pos * this.practicalHeight) + this.y + 1.0F);
    }

    public float getPuckPosition() {
        return (this.puckY - (this.y + 1.0F)) / this.practicalHeight;
    }

}
