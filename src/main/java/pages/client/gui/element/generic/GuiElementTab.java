package pages.client.gui.element.generic;

import java.util.List;

import net.minecraft.client.Minecraft;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementTab extends GuiElement {

    public boolean onLeftSide;
    public boolean onRightSide;
    public GuiElement displayElement;

    public GuiElementTab(int id, int x, int y, IKeyValidator validator, List<GuiElement> listeners, boolean onLeft,
            boolean onRight, GuiElement displayElement, float zLevel) {
        this(id, x, y, 0, 0, validator, listeners, onLeft, onRight, displayElement, zLevel);
    }

    public GuiElementTab(int id, int x, int y, int width, int height, IKeyValidator validator,
            List<GuiElement> listeners, boolean onLeft, boolean onRight, float zLevel) {
        this(id, x, y, width, height, validator, listeners, onLeft, onRight, null, zLevel);
    }

    private GuiElementTab(int id, int x, int y, int width, int height, IKeyValidator validator,
            List<GuiElement> listeners, boolean onLeft, boolean onRight, GuiElement displayElement, float zLevel) {
        super(id, x, y, width, height, validator, listeners, zLevel);
        if (displayElement != null) {
            this.width = displayElement.width + 14;
            this.height = displayElement.height + 7;
            displayElement.x = x + 7;
            displayElement.y = y + 5;
        }
        this.onLeftSide = onLeft;
        this.onRightSide = onRight;

        this.displayElement = this.addElement(displayElement);
    }

    @Override
    public void drawElement(Minecraft mc, int mouseX, int mouseY, float partialTicks) {

        int colorHighlight = this.enabled ? COLOR_WHITE : COLOR_LIGHT_GRAY;
        int colorMain = this.enabled ? COLOR_LIGHT_GRAY : COLOR_GRAY;
        int colorShading = this.enabled ? COLOR_DARK_GRAY : COLOR_DARKER_GRAY;

        // Draw top part of tab.
        this.drawHorizontalLine(this.x + 3, this.x + this.width - 4, this.y, COLOR_BLACK, this.zLevel);
        drawPoint(this.x + this.width - 3, this.y + 1, COLOR_BLACK, this.zLevel);
        drawPoint(this.x + this.width - 2, this.y + 2, COLOR_BLACK, this.zLevel);
        this.drawVerticalLine(this.x + this.width - 1, this.y + 2, this.y + this.height, COLOR_BLACK, this.zLevel);
        this.drawVerticalLine(this.x, this.y + 2, this.y + this.height, COLOR_BLACK, this.zLevel);
        drawPoint(this.x + 1, this.y + 2, COLOR_BLACK, this.zLevel);
        drawPoint(this.x + 2, this.y + 1, COLOR_BLACK, this.zLevel);
        drawRect(this.x + 3, this.y + 3, this.x + this.width - 3, this.y + this.height, colorMain, this.zLevel);
        drawPoint(this.x + this.width - 3, this.y + 2, colorMain, this.zLevel);
        drawRect(this.x + 3, this.y + 1, this.x + this.width - 3, this.y + 3, colorHighlight, this.zLevel);
        drawRect(this.x + 1, this.y + 3, this.x + 3, this.y + this.height, colorHighlight, this.zLevel);
        drawRect(this.x + 2, this.y + 2, this.x + 4, this.y + 4, colorHighlight, this.zLevel);
        drawRect(this.x + this.width - 3, this.y + 3, this.x + this.width - 1, this.y + this.height, colorShading,
                this.zLevel);

        // Draw bottom part of tab.
        if (this.enabled) {

            if (this.onRightSide) {
                this.drawVerticalLine(this.x + this.width - 1, this.y + this.height - 1, this.y + this.height + 3,
                        COLOR_BLACK, this.zLevel);
                drawRect(this.x + 1, this.y + this.height, this.x + 3, this.y + this.height + 1, colorHighlight,
                        this.zLevel);
                drawRect(this.x + 3, this.y + this.height, this.x + this.width - 3, this.y + this.height + 3, colorMain,
                        this.zLevel);
                drawRect(this.x + this.width - 3, this.y + this.height, this.x + this.width - 1,
                        this.y + this.height + 3, colorShading, this.zLevel);
            } else if (this.onLeftSide) {
                this.drawVerticalLine(this.x, this.y + this.height - 1, this.y + this.height + 2, COLOR_BLACK,
                        this.zLevel);
                drawRect(this.x + 1, this.y + this.height, this.x + 3, this.y + this.height + 2, colorHighlight,
                        this.zLevel);
                drawRect(this.x + 3, this.y + this.height, this.x + this.width - 3, this.y + this.height + 4, colorMain,
                        this.zLevel);
                drawRect(this.x + this.width - 3, this.y + this.height, this.x + this.width - 1,
                        this.y + this.height + 2, colorShading, this.zLevel);
                drawPoint(this.x + this.width - 3, this.y + this.height + 2, colorShading, this.zLevel);
            } else {
                drawRect(this.x + 1, this.y + this.height, this.x + 3, this.y + this.height + 1, colorHighlight,
                        this.zLevel);
                drawRect(this.x + 3, this.y + this.height, this.x + this.width - 3, this.y + this.height + 3, colorMain,
                        this.zLevel);
                drawRect(this.x + this.width - 3, this.y + this.height, this.x + this.width - 1,
                        this.y + this.height + 2, colorShading, this.zLevel);
                drawPoint(this.x + this.width - 3, this.y + this.height + 2, colorShading, this.zLevel);
            }
        } else {
            if (this.onRightSide) {
                this.drawVerticalLine(this.x + this.width - 1, this.y + this.height - 1, this.y + this.height + 3,
                        COLOR_BLACK, this.zLevel);
                drawRect(this.x + this.width - 3, this.y + this.height, this.x + this.width - 1,
                        this.y + this.height + 1, colorShading, this.zLevel);
                drawPoint(this.x + this.width - 2, this.y + this.height + 1, colorShading, this.zLevel);
            } else if (this.onLeftSide) {
                this.drawVerticalLine(this.x, this.y + this.height - 1, this.y + this.height + 2, COLOR_BLACK,
                        this.zLevel);
                drawPoint(this.x + 1, this.y + this.height, colorHighlight, this.zLevel);
            }
        }

    }

    @Override
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY, int mouseButton) {
        if (isMouseOver(mouseX, mouseY)) {
            if (!this.isEnabled()) {
                this.notifyListeners();
            }
            return true;
        }
        boolean used = false;
        for (GuiElement element : this.elementList.values()) {
            used = element.mousePressed(mc, mouseX, mouseY, mouseButton);
            if (used) {
                break;
            }
        }
        return used;
    }

    @Override
    public void restore(GuiElement backup) {
        this.setEnabled(backup.enabled);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        boolean displayElementVisible = this.displayElement == null ? false : this.displayElement.isVisible();
        for (GuiElement element : this.elementList.values()) {
            element.setVisible(enabled);
        }
        if (this.displayElement != null) {
            this.displayElement.setVisible(displayElementVisible);
        }
    }

    public interface ITabListener {
        public void tabChanged(int id);
    }

}
