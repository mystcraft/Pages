package pages.client.gui.element.generic;

import java.util.List;

import net.minecraft.client.Minecraft;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementButton extends GuiElement {

    public String label;

    public GuiElementButton(int id, int x, int y, int width, int height, String label, IKeyValidator validator,
            List<GuiElement> listeners, float zLevel) {
        super(id, x, y, width, height, validator, listeners, zLevel);
        this.label = label;
    }

    @Override
    public void drawElement(Minecraft mc, int mouseX, int mouseY, float partialTicks) {

        int highlightColor = COLOR_WHITE;
        int shadowColor = COLOR_DARK_GRAY;
        int mainColor = COLOR_LIGHT_GRAY;
        int textColor = COLOR_BLACK;
        int cornerColor = COLOR_GRAY;
        if (isMouseOver(mouseX, mouseY)) {
            shadowColor = COLOR_DARKER_GRAY;
            mainColor = COLOR_GRAY;
            textColor = COLOR_LIGHT_GRAY;
            cornerColor = COLOR_DARK_GRAY;
        }

        // Draw button.
        drawRect(this.x, this.y, this.x + this.width - 1, this.y + this.height - 1, highlightColor, this.zLevel);
        drawRect(this.x + 1, this.y + 1, this.x + this.width, this.y + this.height, shadowColor, this.zLevel);
        drawRect(this.x + 1, this.y + 1, this.x + this.width - 1, this.y + this.height - 1, mainColor, this.zLevel);
        drawPoint(this.x, this.y + this.height - 1, cornerColor, this.zLevel);
        drawPoint(this.x + this.width - 1, this.y, cornerColor, this.zLevel);

        // Draw label.
        this.drawCenteredString(mc.fontRenderer, this.label, this.x + (this.width / 2), this.y + 3, textColor,
                this.zLevel);

    }

    @Override
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY, int mouseButton) {
        boolean used = super.mousePressed(mc, mouseX, mouseY, mouseButton);
        if (this.isMouseOver(mouseX, mouseY) && this.isVisible() && this.isEnabled()) {
            this.notifyListeners();
        }
        return used;
    }

}
