package pages.client.gui.element.generic;

import java.util.List;

import net.minecraft.client.Minecraft;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementWindow extends GuiElement {

    public GuiElementWindow(int id, int x, int y, int width, int height, IKeyValidator validator,
            List<GuiElement> listeners, float zLevel) {
        super(id, x, y, width, height, validator, listeners, zLevel);
    }

    @Override
    public void drawElement(Minecraft mc, int mouseX, int mouseY, float partialTicks) {

        // Draw black outline.
        this.drawHorizontalLine(this.x + 2, this.x + this.width - 4, this.y, COLOR_BLACK, this.zLevel);
        drawPoint(this.x + this.width - 3, this.y + 1, COLOR_BLACK, this.zLevel);
        drawPoint(this.x + this.width - 2, this.y + 2, COLOR_BLACK, this.zLevel);
        this.drawVerticalLine(this.x + this.width - 1, this.y + 2, this.y + this.height - 2, COLOR_BLACK, this.zLevel);
        drawPoint(this.x + this.width - 2, this.y + this.height - 2, COLOR_BLACK, this.zLevel);
        this.drawHorizontalLine(this.x + 3, this.x + this.width - 3, this.y + this.height - 1, COLOR_BLACK,
                this.zLevel);
        drawPoint(this.x + 2, this.y + this.height - 2, COLOR_BLACK, this.zLevel);
        drawPoint(this.x + 1, this.y + this.height - 3, COLOR_BLACK, this.zLevel);
        this.drawVerticalLine(this.x, this.y + 1, this.y + this.height - 3, COLOR_BLACK, this.zLevel);
        drawPoint(this.x + 1, this.y + 1, COLOR_BLACK, this.zLevel);

        // Draw light gray filling.
        drawRect(this.x + 3, this.y + 3, this.x + this.width - 3, this.y + this.height - 3, COLOR_LIGHT_GRAY,
                this.zLevel);
        drawPoint(this.x + this.width - 3, this.y + 2, COLOR_LIGHT_GRAY, this.zLevel);
        drawPoint(this.x + 2, this.y + this.height - 3, COLOR_LIGHT_GRAY, this.zLevel);

        // Draw white highlights.
        drawRect(this.x + 2, this.y + 1, this.x + this.width - 3, this.y + 3, COLOR_WHITE, this.zLevel);
        drawRect(this.x + 1, this.y + 2, this.x + 3, this.y + this.height - 3, COLOR_WHITE, this.zLevel);
        drawPoint(this.x + 3, this.y + 3, COLOR_WHITE, this.zLevel);

        // Draw dark shading.
        drawRect(this.x + this.width - 3, this.y + 3, this.x + this.width - 1, this.y + this.height - 2,
                COLOR_DARK_GRAY, this.zLevel);
        drawRect(this.x + 3, this.y + this.height - 3, this.x + this.width - 2, this.y + this.height - 1,
                COLOR_DARK_GRAY, this.zLevel);
        drawPoint(this.x + this.width - 4, this.y + this.height - 4, COLOR_DARK_GRAY, this.zLevel);
    }

}
