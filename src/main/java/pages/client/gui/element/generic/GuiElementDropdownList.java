package pages.client.gui.element.generic;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import net.minecraft.client.Minecraft;
import net.minecraft.util.math.MathHelper;
import pages.client.gui.GuiScreenBase;
import pages.client.gui.element.GuiElementListItemString;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementDropdownList extends GuiElement {

    public GuiElement selection;
    public GuiElementButtonDropdown button;
    public GuiElementScrollableList list;
    private int elementId;

    public GuiElementDropdownList(int id, int x, int y, int width, int height, IKeyValidator validator,
            List<GuiElement> listeners, Minecraft mc, List<String> items, float zLevel) {
        super(id, x, y, width, height, validator, listeners, zLevel);
        this.elementId = 100;
        List<GuiElement> list = items.stream().map(item -> new GuiElementListItemString(this.elementId++, 0, 0,
                this.width - 2, validator, Arrays.asList(this), mc, item, COLOR_DARKER_GRAY, zLevel + 2.0F))
                .collect(Collectors.toList());
        this.list = this.addElement(new GuiElementScrollableList(this.elementId++, this.x, this.y + this.height - 1,
                this.width + 16, (14 * MathHelper.clamp(items.size(), 1, 3)) + 2, validator, listeners, 0, mc, list,
                Arrays.asList(GuiScreenBase.screenElement), 3, zLevel + 1.0F));
        this.button = this.addElement(new GuiElementButtonDropdown(this.elementId++, this.x, this.y, this.width, 14,
                ((GuiElementListItemString) this.list.allItems.get(0)).string.value, validator, Arrays.asList(this),
                zLevel));
        this.list.setVisible(this.button.isOpen);
        this.selection = this.list.allItems.get(0);
    }

    @Override
    public void restore(GuiElement backup) {
        if (backup instanceof GuiElementDropdownList) {
            this.selection = ((GuiElementDropdownList) backup).selection;
            this.button.label = ((GuiElementDropdownList) backup).button.label;
            this.list.setVisible(((GuiElementDropdownList) backup).list.isVisible());
        }
    }

    @Override
    public void elementChanged(GuiElement element) {
        if (element instanceof GuiElementButtonDropdown) {
            GuiElementButtonDropdown buttonElement = (GuiElementButtonDropdown) element;
            this.list.setVisible(buttonElement.isOpen);
            setMouseCaptured(buttonElement.isOpen, this.button, this.list, GuiScreenBase.screenElement);
        } else if (element instanceof GuiElementListItemString) {
            GuiElementListItemString itemElement = (GuiElementListItemString) element;
            this.selection = itemElement;
            this.button.label = itemElement.string.value;
            this.button.isOpen = false;
            this.list.setVisible(false);
            setMouseCaptured(false);
        }
    }

    @Override
    public void setVisible(boolean visible) {
        if (this.button != null) {
            super.setVisible(this.button.isOpen);
            this.visible = visible;
            this.button.setVisible(visible);
        }
    }

}
