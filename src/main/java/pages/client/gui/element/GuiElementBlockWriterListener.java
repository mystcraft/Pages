package pages.client.gui.element;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;

import com.google.gson.JsonObject;

import mystlibrary.MystLibrary;
import mystlibrary.grammar.GrammarRule;
import mystlibrary.symbol.SymbolMetadata;
import mystlibrary.symbol.behavior.BlockStateBehavior;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentTranslation;
import pages.client.gui.GuiScreenBlockWriter;
import pages.client.gui.GuiScreenBlockWriter.ElementIds;
import pages.client.gui.element.generic.GuiElement;
import pages.client.gui.element.generic.GuiElementTab;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;
import pages.save.SymbolSaver;

public class GuiElementBlockWriterListener extends GuiElement {

    public GuiScreenBlockWriter listener;

    public GuiElementBlockWriterListener(int id, int x, int y, int width, int height, IKeyValidator validator,
            List<GuiElement> listeners, GuiScreenBlockWriter listener, Float zLevel) {
        super(id, x, y, width, height, validator, listeners, zLevel);
        this.listener = listener;
    }

    @Override
    public void elementChanged(GuiElement element) {
        if (element.id == ElementIds.BUTTON_SAVE.ordinal()) {
            SymbolMetadata metadata = new SymbolMetadata();

            // Get symbol id.
            metadata.name = new ResourceLocation(this.listener.symbolIdField.getText());

            // Get Card Rank.
            metadata.cardRank = this.listener.cardRankField.getText().trim().isEmpty() ? null
                    : Integer.valueOf(this.listener.cardRankField.getText());

            // Get poem;
            metadata.poem = Arrays.asList(this.listener.poemEditor.getWords());

            // Get checked grammar rules.
            for (Entry<String, Integer> entry : this.listener.grammarRuleEditor.getEnabledGrammarRules().entrySet()) {
                metadata.grammarRules.add(new GrammarRule(new ResourceLocation(entry.getKey()), entry.getValue(),
                        new ResourceLocation[0]));
            }

            JsonObject behavior = new JsonObject();

            behavior.addProperty(BlockStateBehavior.BEHAVIOR_ID, BlockStateBehavior.NAME.toString());
            behavior.addProperty("block_id", this.listener.blockState.getBlock().getRegistryName().toString());

            JsonObject properties = new JsonObject();
            for (Entry<String, String> entry : this.listener.propertiesEditor.getProperties()) {
                properties.addProperty(entry.getKey(), entry.getValue());
            }

            behavior.add("properties", properties);

            if (!this.listener.baseInstabilityField.getText().trim().isEmpty()) {
                behavior.addProperty("instability_base", Float.valueOf(this.listener.baseInstabilityField.getText()));
            }

            if (!this.listener.exposedInstabilityField.getText().trim().isEmpty()) {
                behavior.addProperty("instability_exposed",
                        Float.valueOf(this.listener.exposedInstabilityField.getText()));
            }

            JsonObject jsonSymbol = metadata.toJson();
            jsonSymbol.remove("localization");
            jsonSymbol.remove("instability");
            jsonSymbol.add("behavior", behavior);
            File folder = new File(mystlibrary.proxy.CommonProxy.configDirectory,
                    "mystcraft" + File.separator + MystLibrary.MOD_ID + File.separator + "symbols");
            if (!folder.exists()) {
            	folder.mkdirs();
            }
            File file = new File(mystlibrary.proxy.CommonProxy.configDirectory,
                    "mystcraft" + File.separator + MystLibrary.MOD_ID + File.separator + "symbols" + File.separator
                            + metadata.name.getResourcePath().toLowerCase().replace(" ", "_") + ".json");
            boolean saved = SymbolSaver.saveSymbolToDisk(jsonSymbol, file, false);
            if (saved) {
                this.listener.mc.player.sendMessage(
                        new TextComponentTranslation("message.pages.block_symbol_saved", metadata.name.toString()));
            } else {
                this.listener.mc.player.sendMessage(
                        new TextComponentTranslation("message.pages.block_symbol_not_saved", metadata.name.toString()));
            }
        } else if (element instanceof GuiElementTab) {
            for (GuiElement oneElement : this.listener.elementList.values()) {
                if (oneElement instanceof GuiElementTab) {
                    oneElement.setEnabled(oneElement.id == element.id ? true : false);
                }
            }
        }
    }

}
