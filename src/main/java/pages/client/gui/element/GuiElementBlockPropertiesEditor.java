package pages.client.gui.element;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import net.minecraft.client.Minecraft;
import net.minecraft.util.text.TextComponentTranslation;
import pages.client.gui.element.generic.GuiElement;
import pages.client.gui.element.generic.GuiElementString;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementBlockPropertiesEditor extends GuiElement {

    public List<GuiElementBlockProperty> propertiesList = new ArrayList<GuiElementBlockProperty>();

    public GuiElementBlockPropertiesEditor(int id, int x, int y, int width, int height, IKeyValidator validator,
            List<GuiElement> listeners, Minecraft mc, Map<String, List<String>> properties, float zLevel) {
        super(id, x, y, width, height, validator, listeners, zLevel);
        int elementId = 0;
        this.addElement(new GuiElementString(elementId++, x, y, validator, listeners, mc,
                new TextComponentTranslation("gui.pages.block_writer.element.properties").getFormattedText(),
                COLOR_BLACK, this.zLevel));
        int currentY = this.y;
        int currentNumber = 1;
        for (Entry<String, List<String>> property : properties.entrySet()) {
            GuiElementBlockProperty currentProperty = new GuiElementBlockProperty(elementId++, this.x, currentY += 16,
                    this.width, 14, validator, listeners, mc, currentNumber++, property.getKey(), property.getValue(),
                    this.zLevel);
            this.propertiesList.add(currentProperty);
        }
        for (GuiElement element : this.propertiesList) {
            this.addElement(element);
        }
    }

    public Set<Entry<String, String>> getProperties() {
        return this.propertiesList.stream().map(property -> property.getProperty()).collect(Collectors.toSet());
    }

}
