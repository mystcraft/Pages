package pages.client.gui.element;

import java.util.Arrays;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.util.text.TextComponentTranslation;
import pages.client.gui.element.generic.GuiElement;
import pages.client.gui.element.generic.GuiElementString;
import pages.client.gui.element.generic.GuiElementTextField;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;
import pages.client.gui.element.generic.keyvalidator.KeyValidators;

public class GuiElementPoemEditor extends GuiElement {

    GuiElementTextField textFieldOne;
    GuiElementTextField textFieldTwo;
    GuiElementTextField textFieldThree;
    GuiElementTextField textFieldFour;

    public GuiElementPoemEditor(int id, int x, int y, int width, int height, IKeyValidator validator,
            List<GuiElement> listeners, Minecraft mc, String wordOne, String wordTwo, String wordThree, String wordFour,
            float zLevel) {
        super(id, x, y, width, height, validator, listeners, zLevel);
        GuiElementString stringElement = this.addElement(new GuiElementString(0, this.x, this.y + 2, validator,
                listeners, mc, new TextComponentTranslation("gui.pages.block_writer.element.poem").getFormattedText(),
                COLOR_BLACK, zLevel));
        GuiElementNarayaniPoem poemElement = this
                .addElement(new GuiElementNarayaniPoem(1, this.x + this.width - 32, this.y + ((this.height - 32) / 2),
                        32, 32, validator, listeners, wordOne, wordTwo, wordThree, wordFour, zLevel));
        this.textFieldOne = this.addElement(new GuiElementTextField(2, this.x + stringElement.width + 4, this.y,
                this.width - stringElement.width - 32 - 8, KeyValidators::isAnything, Arrays.asList(poemElement), mc,
                COLOR_WHITE, wordOne, zLevel));
        this.textFieldTwo = this.addElement(new GuiElementTextField(3, this.x + stringElement.width + 4, this.y + 14,
                this.width - stringElement.width - 32 - 8, KeyValidators::isAnything, Arrays.asList(poemElement), mc,
                COLOR_WHITE, wordTwo, zLevel));
        this.textFieldThree = this.addElement(new GuiElementTextField(4, this.x + stringElement.width + 4, this.y + 28,
                this.width - stringElement.width - 32 - 8, KeyValidators::isAnything, Arrays.asList(poemElement), mc,
                COLOR_WHITE, wordThree, zLevel));
        this.textFieldFour = this.addElement(new GuiElementTextField(5, this.x + stringElement.width + 4, this.y + 42,
                this.width - stringElement.width - 32 - 8, KeyValidators::isAnything, Arrays.asList(poemElement), mc,
                COLOR_WHITE, wordFour, zLevel));
    }

    public String[] getWords() {
        return new String[] { this.textFieldOne.getText(), this.textFieldTwo.getText(), this.textFieldThree.getText(),
                this.textFieldFour.getText() };
    }

}
