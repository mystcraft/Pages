package pages.client.gui.element;

import java.util.List;

import net.minecraft.client.Minecraft;
import pages.client.gui.element.generic.GuiElement;
import pages.client.gui.element.generic.GuiElementCheckBox;
import pages.client.gui.element.generic.GuiElementStringTruncated;
import pages.client.gui.element.generic.GuiElementTextField;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;
import pages.client.gui.element.generic.keyvalidator.KeyValidators;

public class GuiElementListItemGrammarRule extends GuiElement {

    public GuiElementCheckBox checkbox;
    public GuiElementTextField textfield;
    public GuiElementStringTruncated title;

    public GuiElementListItemGrammarRule(int id, int x, int y, int width, IKeyValidator validator,
            List<GuiElement> listeners, Minecraft mc, String title, float zLevel) {
        super(id, x, y, width, 18, validator, listeners, zLevel);
        this.checkbox = this.addElement(new GuiElementCheckBox(0, x + 4, y + 4, 10, 10, validator, listeners, zLevel));
        this.textfield = this.addElement(new GuiElementTextField(2, x + width - 4 - 24, y + 3, 24,
                KeyValidators::isInteger, listeners, mc, COLOR_WHITE, "", zLevel));
        this.title = this.addElement(new GuiElementStringTruncated(1, x + 16, y + 5,
                width - this.checkbox.width - this.textfield.width - 3 - 2 - 2 - 3, validator, listeners, mc, title,
                COLOR_DARKER_GRAY, zLevel));
    }

    @Override
    public void drawElement(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        if (this.isVisible()) {
            drawRect(this.x, this.y, this.x + this.width - 1, this.y + this.height - 1, COLOR_WHITE, this.zLevel);
            drawRect(this.x + 1, this.y + 1, this.x + this.width, this.y + this.height, COLOR_DARKER_GRAY, this.zLevel);
            drawRect(this.x + 1, this.y + 1, this.x + this.width - 1, this.y + this.height - 1, COLOR_LIGHT_GRAY,
                    this.zLevel);
            drawPoint(this.x, this.y + this.height - 1, COLOR_LIGHT_GRAY, this.zLevel);
            drawPoint(this.x + this.width - 1, this.y, COLOR_LIGHT_GRAY, this.zLevel);
        }
    }

}
