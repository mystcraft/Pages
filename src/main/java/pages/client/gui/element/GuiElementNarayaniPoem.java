package pages.client.gui.element;

import java.util.List;

import com.xcompwiz.mystcraft.api.MystObjects;

import mystlibrary.MystLibrary;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import pages.Pages;
import pages.client.gui.element.generic.GuiElement;
import pages.client.gui.element.generic.GuiElementTextField;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementNarayaniPoem extends GuiElement {

    protected static final ResourceLocation TEXTURE_PAGE = new ResourceLocation(MystObjects.MystcraftModId,
            "textures/items/page_background.png");

    public String wordOne;
    public String wordTwo;
    public String wordThree;
    public String wordFour;

    public GuiElementNarayaniPoem(int id, int x, int y, int width, int height, IKeyValidator validator,
            List<GuiElement> listeners, String wordOne, String wordTwo, String wordThree, String wordFour,
            float zLevel) {
        super(id, x, y, width, height, validator, listeners, zLevel);
        this.wordOne = wordOne;
        this.wordTwo = wordTwo;
        this.wordThree = wordThree;
        this.wordFour = wordFour;
    }

    @Override
    public void drawElement(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        mc.getTextureManager().bindTexture(TEXTURE_PAGE);
        drawScaledCustomSizeModalRect(this.x, this.y, 0, 0, 1, 1, this.width, this.height, 1, 1);
        float wordWidth = this.width / 2.5F;
        MystLibrary.mystcraft.render.drawWord(this.x + ((this.width - wordWidth) / 2.0F), this.y, this.zLevel,
                wordWidth, this.wordOne);
        MystLibrary.mystcraft.render.drawWord(this.x + (this.width - wordWidth),
                this.y + ((this.width - wordWidth) / 2.0F), this.zLevel, wordWidth, this.wordTwo);
        MystLibrary.mystcraft.render.drawWord(this.x + ((this.width - wordWidth) / 2.0F),
                this.y + (this.width - wordWidth), this.zLevel, wordWidth, this.wordThree);
        MystLibrary.mystcraft.render.drawWord(this.x, this.y + ((this.width - wordWidth) / 2.0F), this.zLevel,
                wordWidth, this.wordFour);
        super.drawElement(mc, mouseX, mouseY, partialTicks);
    }

    @Override
    public void elementChanged(GuiElement element) {
        if (element instanceof GuiElementTextField) {
            GuiElementTextField textFieldElement = (GuiElementTextField) element;
            switch (textFieldElement.id) {
                case 2:
                    this.wordOne = textFieldElement.getText();
                    break;
                case 3:
                    this.wordTwo = textFieldElement.getText();
                    break;
                case 4:
                    this.wordThree = textFieldElement.getText();
                    break;
                case 5:
                    this.wordFour = textFieldElement.getText();
                    break;
            }
        } else {
            Pages.logger.error("Element being listened to (ID: " + String.valueOf(element.id)
                    + ") is not a GuiElementTextField. Cannot update displayed poem.");
        }
    }

}
