package pages.client.gui.element;

import java.util.List;
import java.util.Map.Entry;

import com.google.common.collect.Maps;

import net.minecraft.client.Minecraft;
import pages.client.gui.element.generic.GuiElement;
import pages.client.gui.element.generic.GuiElementDropdownList;
import pages.client.gui.element.generic.GuiElementString;
import pages.client.gui.element.generic.keyvalidator.IKeyValidator;

public class GuiElementBlockProperty extends GuiElement {

    public int number;
    public String name;
    public GuiElementDropdownList dropdown;

    public GuiElementBlockProperty(int id, int x, int y, int width, int height, IKeyValidator validator,
            List<GuiElement> listeners, Minecraft mc, int number, String name, List<String> values, float zLevel) {
        super(id, x, y, width, height, validator, listeners, zLevel);
        this.name = name == null ? "" : name;
        GuiElementString title = this.addElement(new GuiElementString(0, this.x, this.y + 3, validator, listeners, mc,
                String.valueOf(number) + ". " + this.name + " = ", COLOR_BLACK, zLevel));
        int buttonWidth = this.width - title.width;
        this.dropdown = this.addElement(new GuiElementDropdownList(1, this.x + title.width, this.y, buttonWidth, 14,
                validator, listeners, mc, values, zLevel));
    }

    public Entry<String, String> getProperty() {
        return Maps.immutableEntry(this.name, ((GuiElementListItemString) this.dropdown.selection).string.value);
    }

}
