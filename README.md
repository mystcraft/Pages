# Pages
***Pages*** is an *Minecraft* mod that adds addiitonal symbols/pages for the mod *[Mystcraft](https://www.curseforge.com/minecraft/mc-mods/mystcraft)*. It depends on the library mod *[Myst Library](https://www.curseforge.com/minecraft/mc-mods/mystlibrary)* for the actual symbol-adding code. The CurseForge project is located at 
https://www.curseforge.com/minecraft/mc-mods/pages.

You can add Pages to your dev environment by adding

```
maven {
    name = "curseforge"
    url = "https://minecraft.curseforge.com/api/maven/"
}
```
to the **repositories** section of your **build.gradle** file if it is not already present. Then refer to it in your **dependencies** section as
```
pages:pages:1.12.2:2.0.0
```
